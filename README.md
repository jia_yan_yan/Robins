


## 简介 - Causal Inference Book

> **Book：** Hernán MA, Robins JM (2020).  **Causal Inference: What If**. Boca Raton: Chapman & Hall/CRC. 

![](https://images.gitee.com/uploads/images/2020/0228/175928_beb2892e_1522177.png)

> 「**因果推断**」最近很火，让我们很是 **恼火**!

&emsp; 

今天要介绍的这本书叫 **《Causal Inference: What If》**，是统计学大牛 J. Robins 和 Hernán 共同完成的一本颇具影响力的书。这本书已经面世好几年的时间了，目前放在他们主页上的是最新修订版。两位作者非常慷慨，直接在自己的主页上放了书稿的 PDF 原本。

来自不同领域的学者们也对这本书给予了很大的支持。提供了这本书部分章节的实现代码，包括 Stata, R, Python, SAS。

其中，Stata 实现过程由 Eleanor Murray 提供。 

第 1-10 章重点讲述一些基本概念，通过大量的简单实例和图形的方式，深入浅出地介绍了因果推断中的核心概念和方法。第 11-18 章讲述了各种各样用于进行因果推断的模型，包括工具变量法 (IV)、倾向得分匹配分析 (PSM)、调节效应、结构方程等。第 19-25 章介绍了较为复杂的情形，如面板数据、
动态处理效应、反馈效应等。 

为了便于大家学习这本书里面的内容，我们从作者的主页以及 Github 仓库上，把所有相关的资料都下载下来并整理成了目前的码云仓库，以便于国内用户访问。 

## 书稿、数据和程序

- Hernán MA, Robins JM (2020). Causal Inference: What If. Boca Raton: Chapman & Hall/CRC. [[PDF在线浏览]](https://quqi.gblhgk.com/s/880197/eQHgb0bnGgYeafcp)，[[作者书稿主页]](https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/)
- **Stata 实现过程：** Chp 11-17 各章的 Stata 实现代码来自 [Eleanor Murray - Github](https://github.com/eleanormurray/causalinferencebook_stata)。
- **R, Python, SAS 实现过程 ：** 请访问 [[作者书稿主页]](https://www.hsph.harvard.edu/miguel-hernan/causal-inference-book/)，包括书中第 11-17 章中所用的数据和实现过程。 

## 使用方法

进入码云仓库后，点击橘红色【克隆/下载】按钮，即可将仓库中所有文件下载到本地电脑中。

在 Stata 中打开【_Main.do】文档后，按提示执行命令即可打开各章对应的 dofiles。


&emsp; 
---

## 附：仓库、书中目录节选

![](https://images.gitee.com/uploads/images/2020/0228/175928_c715e450_1522177.png)

![](https://images.gitee.com/uploads/images/2020/0228/175928_dd118ae6_1522177.png)

![](https://images.gitee.com/uploads/images/2020/0228/175928_231b53dc_1522177.png)

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- &#x1F33A; **你的颈椎还好吗？** 为了保护颈椎，您可以前往 [::连享会-主页::](https://www.lianxh.cn) (在浏览器地址栏中输入 lianxh.cn 即可) 或 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) (在知乎中搜索「连享会」即可) 查看往期推文，并将上述网址添加到浏览器收藏夹中或把您喜欢的推文【收藏】起来。
- &#x1F4C1; **往期推文分类查看：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- &#x1F4A5; **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`DID，RDD，PSM，面板，IV，合成控制法，内生性, Probit, plus，Profile, Bootstrap, MC, 交乘项, 平方项, 工具, 软件, Sai2, gInk, Annotator, 手写批注, 直击面板数据, 连老师, 直播, 空间计量, 爬虫, 文本分析, 正则, Markdown幻灯片, marp, 盈余管理, ` ……。


---

![欢迎加入Stata连享会(公众号: StataChina)](https://images.gitee.com/uploads/images/2020/0228/175928_38d04783_1522177.png)



